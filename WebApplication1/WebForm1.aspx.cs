﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Domain;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        private CarDealer cd;
        private TableRow tRow;
        private TableCell tCell;

        protected void Page_Load(object sender, EventArgs e)
        {
            

            TextBox1.Enabled = false;
            

            cd = DFacade.CarDealerObject;
            

            Table1.GridLines = GridLines.Both;

            Table1.Rows.Clear();

            tRow = new TableRow();

            tCell = new TableCell();
            tCell.Text = "Id";
            tRow.Cells.Add(tCell);

            tCell = new TableCell();
            tCell.Text = "Model";
            tRow.Cells.Add(tCell);

            tCell = new TableCell();
            tCell.Text = "Color";
            tRow.Cells.Add(tCell);

            tCell = new TableCell();
            tCell.Text = "Price";
            tRow.Cells.Add(tCell);

            tCell = new TableCell();
            tCell.Text = "State";
            tRow.Cells.Add(tCell);


            Table1.Rows.Add(tRow);
            
            if (!IsPostBack || ViewState["Buttons"] == null)
            {
                return;
            }

            if((int)ViewState["Buttons"] == 1)
            {
                
                loadCars();
            }else if((int)ViewState["Buttons"] == 2)
            {
                
                loadTrucks();
            }
            
            
        }
        
        private void loadCars()
        {
            foreach (Car item in cd.GetCars())
            {

                tRow = new TableRow();

                tCell = new TableCell();
                Button b = new Button();
                b.Click += new EventHandler(B_Click);
                b.Text = item.Id + "";
                
                tCell.Controls.Add(b);
                tRow.Cells.Add(tCell);

                tCell = new TableCell();
                tCell.Text = item.Model + "";
                tRow.Cells.Add(tCell);

                tCell = new TableCell();
                tCell.Text = item.Color + "";
                tRow.Cells.Add(tCell);

                tCell = new TableCell();
                tCell.Text = item.Price + "";
                tRow.Cells.Add(tCell);

                tCell = new TableCell();
                tCell.Text = item.State + "";
                tRow.Cells.Add(tCell);

                Table1.Rows.Add(tRow);

            }


        }
        

        protected void BrowseCars_Click(object sender, EventArgs e)
        {
            //loadCars();
            ViewState["Buttons"] = 1;
            ViewState["type"] = "Car";


        }

        protected void B_Click(object sender, EventArgs e)
        {
            
            Button temp = (Button)sender;

            TextBox1.Text = temp.Text;


            IVehicle v = null;

            string s = (string) ViewState["type"];
            

            if (s.Equals("Car"))
            {
                v = cd.GetVehicle(Convert.ToInt32(temp.Text), typeof(Car));
            }
            else if (s.Equals("Truck"))
            {
                v = cd.GetVehicle(Convert.ToInt32(temp.Text), typeof(Truck));
            }

            if(s == null)
            {
                throw new Exception();
            }
            v.State = VehicleState.Sold;
            
            cd.Save(v);
            
        }

        private void loadTrucks()
        {
            foreach (Truck item in cd.GetTrucks())
            {

                tRow = new TableRow();

                tCell = new TableCell();
                Button b = new Button();
                b.Click += new EventHandler(B_Click);
                b.Text = item.Id + "";
                tCell.Controls.Add(b);
                tRow.Cells.Add(tCell);

                tCell = new TableCell();
                tCell.Text = item.Model + "";
                tRow.Cells.Add(tCell);

                tCell = new TableCell();
                tCell.Text = item.Color + "";
                tRow.Cells.Add(tCell);

                tCell = new TableCell();
                tCell.Text = item.Price + "";
                tRow.Cells.Add(tCell);

                tCell = new TableCell();
                tCell.Text = item.State + "";
                tRow.Cells.Add(tCell);

                Table1.Rows.Add(tRow);

            }
            
        }

        protected void BrowseTrucks_Click(object sender, EventArgs e)
        {
            //loadTrucks();
            ViewState["Buttons"] = 2;
            ViewState["type"] = "Truck";
        }

    }
}