﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
/**
This project was part of Lesson2-5 exercise
**/
namespace FileSystemWatcherExercise
{
    class Program
    {
        private static FileSystemWatcher watcher;
        static void Main(string[] args)
        {

            List<Car> cars = ParseFile("Trucks.csv");

            foreach (var car in cars)
            {
                Console.WriteLine(car);
            }


            /*FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = "WatchFolder";
            watcher.Filter = "*.csv";
            watcher.Created += new FileSystemEventHandler(watcher_FileCreated);
            watcher.EnableRaisingEvents = true;*/
             watcher = new FileSystemWatcher();
            watcher.Path = "WatchFolder";
            watcher.NotifyFilter = NotifyFilters.FileName;
            watcher.Filter = "*.*";
            watcher.Created += new FileSystemEventHandler(watcher_FileCreated);
            watcher.EnableRaisingEvents = true;

            Console.ReadLine();
        }

        static List<Car> ParseFile(String path)
        {
            var reader = new StreamReader(@path);
            List<Car> cars = new List<Car>();
            int lineCount = 1;
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                try
                {
                    var values = line.Split(';');

                    int id = Convert.ToInt32(values[0]);
                    string color = values[1];
                    string model = values[2];
                    int price = Convert.ToInt32(values[3]);
                    VehicleState state = (VehicleState)Enum.Parse(typeof(VehicleState), values[4]);
                    cars.Add(new Car(id, color, model, price, state));
                }
                catch (Exception e)
                {
                    Console.WriteLine(@"Could not parse line #" + lineCount);
                }

                lineCount++;
            }
            reader.Close();
            return cars;
        }

        static void watcher_FileCreated(object sender, FileSystemEventArgs e)
        {
            
            Console.WriteLine("\t******A new import-file has been detected****");
            List<Car> cars = ParseFile(e.FullPath);

            //TODO: Pass on cars to persistence

            Console.WriteLine(e.FullPath);

            Console.WriteLine("Imported the following Cars:");

            foreach (var car in cars)
            {
                Console.WriteLine(car);
            }

            Console.WriteLine("Deleting file...");
            File.Delete(e.FullPath);
            Console.WriteLine(e.FullPath +" has been successfully deleted!");
        }
    }
}
