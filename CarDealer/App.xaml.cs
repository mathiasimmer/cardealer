﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using CarDealer.ViewModels;
using CarDealer.Views;
using Domain;

namespace CarDealer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            //var windows = new CarsView();
            //AllCarsViewModel VM = new AllCarsViewModel();
            //windows.DataContext = VM;
            //windows.Show();
            MainWindow window = new MainWindow();

            var viewModel = new MainWindowViewModel("Data/customers.xml", DFacade.CarDealerObject);


            //handle closing of ViewModels
            EventHandler handler = null;
            handler = delegate
            {
                viewModel.RequestClose -= handler;
                window.Close();
            };
            viewModel.RequestClose += handler;

            window.DataContext = viewModel;

            window.Show();

        }
    }
}
