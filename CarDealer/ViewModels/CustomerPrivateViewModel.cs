﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using Domain;

namespace CarDealer.ViewModels
{
    /// <summary>
    /// Author:Mathias
    /// </summary>
    public class CustomerPrivateViewModel : WorkspaceViewModel, IDataErrorInfo
    {
        private readonly Domain.CarDealer _carDealer;
        private readonly Private _newPrivate;
        RelayCommand _saveCommand;

        public CustomerPrivateViewModel(Private newPrivate, Domain.CarDealer carDealer)
        {

            if (newPrivate == null)
                throw new ArgumentNullException("newPrivate");

            if (carDealer == null)
                throw new ArgumentNullException("carDealer");

            _newPrivate = newPrivate;
            _carDealer = carDealer;

            base.DisplayName = Strings.CustomerPrivateViewModel_DisplayName;
        }

        public string Name
        {
            get { return _newPrivate.Name; }
            set
            {
                if (value == _newPrivate.Name)
                    return;

                _newPrivate.Name = value;

                base.OnPropertyChanged("Name");
            }
        }

        public string Address
        {
            get { return _newPrivate.Address; }
            set
            {
                if (value == _newPrivate.Address)
                    return;

                _newPrivate.Address = value;

                base.OnPropertyChanged("Address");
            }
        }

        public int Phone
        {
            get { return _newPrivate.Phone; }
            set
            {
                if (value == _newPrivate.Phone)
                    return;
                _newPrivate.Phone = value;

                base.OnPropertyChanged("Phone");
            }
        }

        public int Age
        {
            get { return _newPrivate.Age; }
            set
            {
                if (value == _newPrivate.Age)
                    return;
                _newPrivate.Age = value;

                base.OnPropertyChanged("Age");
            }
        }


        public string[] GenderOptions
        {
            get
            {

                var enumValues = Enum.GetValues(typeof (Gender));

                string[] values = enumValues.OfType<object>().Select(o => o.ToString()).ToArray();


                //string[] newValues = new string[values.Length + 1];
                //newValues[0] = Strings.Unspecified;
                //Array.Copy(values, 0, newValues, 1, values.Length);

                //return newValues;
                return values;
            }
        }


        public string Gender
        {
            get { return _newPrivate.Gender.ToString(); }
            set
            {
                if (value == _newPrivate.Gender.ToString() || String.IsNullOrEmpty(value))
                    return;

                _newPrivate.Gender = (Gender) Enum.Parse(typeof(Gender), value);

                base.OnPropertyChanged("Gender");

            }
        }


        bool CanSave
        {
            get
            {
                return !_newPrivate.Gender.ToString().Equals("") && _newPrivate.IsValid;
            }
        }

        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        param => this.Save(),
                        param => this.CanSave
                        );
                }
                return _saveCommand;
            }
        }


        public void Save()
        {
            if (!_newPrivate.IsValid)
                throw new InvalidOperationException("CustomerPrivateViewModel: Cannot save - Customer invalid");

            if (this.IsNewCustomer)
                _carDealer.AddCustomer(_newPrivate);

            //base.OnPropertyChanged("DisplayName");
        }

        bool IsNewCustomer
        {
            get { return !_carDealer.ContainsCustomer(_newPrivate); }
        }

        public string Error
        {
            get { return (_newPrivate as IDataErrorInfo).Error; }
        }

        public string this[string propertyName]
        {
            get
            {
                string error = (_newPrivate as IDataErrorInfo)[propertyName];

                // Dirty the commands registered with CommandManager,
                // such as our Save command, so that they are queried
                // to see if they can execute now.
                CommandManager.InvalidateRequerySuggested();

                return error;
            }
        }
    }
}