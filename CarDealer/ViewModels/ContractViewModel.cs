﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using CarDealer.Views;
using Domain;

namespace CarDealer.ViewModels
{
    /// <summary>
    /// Author:Mathias
    /// </summary>
    public class ContractViewModel
    {
        private Contract _contract;
        private Domain.CarDealer _carDealer;
        private Private _customer;

        public ContractViewModel(Contract contract, Private customer, Domain.CarDealer carDealer)
        {
            this._contract = contract;
            this._customer = customer;
            this._carDealer = carDealer;
        }

        public void Init()
        {
            ContractView view = new ContractView();
            view.DataContext = this;
            view.Show();
        }

        public List<Car> Vehicles
        {
            get { return _carDealer.GetCars(); }
        }

        public Car SelectedVehicle { get; set; }

        public DateTime Date
        {
            get { return _contract.Date; }
            set
            {
                if (value == _contract.Date)
                    return;

                _contract.Date = value;
                //base.OnPropertyChanged("Date");
            }
        }

        public RelayCommand _saveCommand { get; private set; }

        bool CanSave
        {
            get
            {
                return SelectedVehicle != null;
            }
        }

        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        param => this.Save(),
                        param => this.CanSave
                        );
                }
                return _saveCommand;
            }
        }

        public void Save()
        {

            //if (!_newLeasing.IsValid)
            //    throw new InvalidOperationException("VehicleTruckViewModel: Cannot save - Truck invalid");

            _contract.Vehicle = SelectedVehicle;

            _customer.AddContract(_contract);

            //base.OnPropertyChanged("DisplayName");
        }
    }
}