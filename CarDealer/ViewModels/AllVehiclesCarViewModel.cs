﻿using System;
using System.Collections.Generic;
using Domain;

namespace CarDealer.ViewModels
{
    /// <summary>
    /// Author:Mathias
    /// </summary>
    public class AllVehiclesCarViewModel : WorkspaceViewModel
    {
        private Domain.CarDealer _carDealer;

        public AllVehiclesCarViewModel(Domain.CarDealer carDealer)
        {
            if (carDealer == null)
                throw new ArgumentNullException("carDealer");

            this._carDealer = carDealer;

            base.DisplayName = Strings.MainWindowViewModel_Command_ViewAllCars;
        }

        public List<Car> Vehicles
        {
            get { return _carDealer.GetCars(); }
        } 
    }
}