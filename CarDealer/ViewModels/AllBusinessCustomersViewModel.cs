﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using CarDealer.Views;
using Domain;

namespace CarDealer.ViewModels
{
    /// <summary>
    /// Author:Mathias
    /// </summary>
    public class AllBusinessCustomersViewModel : WorkspaceViewModel
    {
        private readonly Domain.CarDealer _carDealer;
        public Business SelectedCustomer { get; set; }
        RelayCommand _editLeasingCommand;

        public ObservableCollection<CustomerBusinessViewModel> AllBusinessCustomers { get; private set; }

        public AllBusinessCustomersViewModel(Domain.CarDealer carDealer)
        {
            if (carDealer == null)
                throw new ArgumentNullException("carDealer");

            this._carDealer = carDealer;

            //_carDealer.CustomerAdded += this.onCustomerAdded;

            //this.PopulateCustomers();

            base.DisplayName = "Show all B.C.";
        }

        //void PopulateCustomers()
        //{
        //    List<CustomerBusinessViewModel> all =
        //        (from cust in _carDealer.GetBusinessCustomers()
        //         select new CustomerBusinessViewModel(cust, _carDealer)).ToList();

        //    //foreach (CustomerBusinessViewModel cvm in all)
        //    //    cvm.PropertyChanged += this.OnCustomerViewModelPropertyChanged;

        //    this.AllBusinessCustomers = new ObservableCollection<CustomerBusinessViewModel>(all);
        //    //this.AllBusinessCustomers.CollectionChanged += this.OnCollectionChanged;
        //}


        public List<Business> Customers
        {
            get
            {
                return _carDealer.GetBusinessCustomers();

            }
            set { }
        }


        bool CanEditLeasing
        {
            get { return SelectedCustomer != null; }
        }

        public ICommand EditLeasingCommand
        {
            get
            {
                if (_editLeasingCommand == null)
                {
                    _editLeasingCommand = new RelayCommand(
                        param => this.EditLeasing(),
                        param => this.CanEditLeasing
                        );
                }
                return _editLeasingCommand;
            }
        }


        public void EditLeasing()
        {
            //Todo
            ShowLeasingListViewModel vm = new ShowLeasingListViewModel(SelectedCustomer, _carDealer);
            vm.Init();

            //if (!_newPrivate.IsValid)
            //    throw new InvalidOperationException("CustomerPrivateViewModel: Cannot save - Customer invalid");

            //if (this.IsNewCustomer)
            //    _carDealer.AddCustomer(_newPrivate);

            //base.OnPropertyChanged("DisplayName");
        }




        #region Event Handling Methods

        //void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        //{
        //    if (e.NewItems != null && e.NewItems.Count != 0)
        //        foreach (CustomerViewModel custVM in e.NewItems)
        //            custVM.PropertyChanged += this.OnCustomerViewModelPropertyChanged;

        //    if (e.OldItems != null && e.OldItems.Count != 0)
        //        foreach (CustomerViewModel custVM in e.OldItems)
        //            custVM.PropertyChanged -= this.OnCustomerViewModelPropertyChanged;
        //}

        //void OnCustomerViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        //{
        //    string IsSelected = "IsSelected";

        //}

        //void onCustomerAdded(object sender, CustomerAddedEventArgs e)
        //{
        //    var viewModel = new CustomerBusinessViewModel(e.NewCustomer, _carDealer);
        //    this.AllBusinessCustomers.Add(viewModel);
        //}

        #endregion // Event Handling Methods
    }
}