﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using CarDealer.Views;
using Domain;


namespace CarDealer.ViewModels
{
    /// <summary>
    /// Author:Mathias
    /// </summary>
    public class ShowLeasingListViewModel
    {
        private readonly Business _customer;
        public Leasing SelectedLeasing { get; set; }
        RelayCommand _addLeasingCommand;
        private Domain.CarDealer _carDealer;

        public ShowLeasingListViewModel(Business selectedCustomer, Domain.CarDealer carDealer)
        {
            _customer = selectedCustomer;
            _carDealer = carDealer;
        }

        public void Init()
        {
            ShowLeasingListView listView = new ShowLeasingListView();
            listView.DataContext = this;
            listView.Show();
            
        }

        public List<Leasing> Leasings
        {
            get { return _customer.Trucks; }
        }


        bool CanAddLeasing
        {
            get { return true; }
        }

        public ICommand AddLeasingCommand
        {
            get
            {
                if (_addLeasingCommand == null)
                {
                    _addLeasingCommand = new RelayCommand(
                        param => this.AddLeasing(),
                        param => this.CanAddLeasing
                        );
                }
                return _addLeasingCommand;
            }
        }


        public void AddLeasing()
        {
            Leasing newLeasing = new Leasing();
            newLeasing.Date = DateTime.Now;
            LeasingViewModel vm = new LeasingViewModel(newLeasing, _customer, _carDealer);

            vm.Init();

            //if (!_newPrivate.IsValid)
            //    throw new InvalidOperationException("CustomerPrivateViewModel: Cannot save - Customer invalid");

            //if (this.IsNewCustomer)
            //    _carDealer.AddCustomer(_newPrivate);

            //base.OnPropertyChanged("DisplayName");
        }


    }
}