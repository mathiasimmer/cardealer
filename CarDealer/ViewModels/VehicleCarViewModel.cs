﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using Domain;

namespace CarDealer.ViewModels
{
    /// <summary>
    /// Author:Mathias
    /// </summary>
    public class VehicleCarViewModel : WorkspaceViewModel, IDataErrorInfo
    {
        private Car _newCar;
        private readonly Domain.CarDealer _carDealer;
        RelayCommand _saveCommand;

        public VehicleCarViewModel(Car newCar, Domain.CarDealer carDealer)
        {
            _newCar = newCar;
            _carDealer = carDealer;

            base.DisplayName = Strings.MainWindowViewModel_Command_CreateNewCar;
        }

        public string Model
        {
            get { return _newCar.Model; }
            set
            {
                if (value == _newCar.Model)
                    return;

                _newCar.Model = value;

                base.OnPropertyChanged("Model");
            }
        }

        public int Price
        {
            get { return _newCar.Price; }
            set
            {
                if (value == _newCar.Price)
                    return;

                _newCar.Price = value;

                base.OnPropertyChanged("Price");
            }
        }

        public string Color
        {
            get { return _newCar.Color; }
            set
            {
                if (value == _newCar.Color)
                    return;

                _newCar.Color = value;

                base.OnPropertyChanged("Color");
            }
        }


        public string State
        {
            get { return _newCar.State.ToString(); }
            set
            {
                if (value == _newCar.State.ToString() || String.IsNullOrEmpty(value))
                    return;

                _newCar.State = (VehicleState)Enum.Parse(typeof(VehicleState), value);

                base.OnPropertyChanged("State");

            }
        }

        public string[] StateOptions
        {
            get
            {

                var enumValues = Enum.GetValues(typeof(VehicleState));

                string[] values = enumValues.OfType<object>().Select(o => o.ToString()).ToArray();


                //string[] newValues = new string[values.Length + 1];
                //newValues[0] = Strings.Unspecified;
                //Array.Copy(values, 0, newValues, 1, values.Length);

                //return newValues;
                return values;
            }
        }


        bool CanSave
        {
            get
            {
                return !_newCar.State.ToString().Equals("") && _newCar.IsValid;
            }
        }

        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        param => this.Save(),
                        param => this.CanSave
                        );
                }
                return _saveCommand;
            }
        }


        public void Save()
        {
            if (!_newCar.IsValid)
                throw new InvalidOperationException("VehicleCarViewModel: Cannot save - Car invalid");

            if (this.IsNewVehicle)
                _carDealer.AddVehicle(_newCar);

            //base.OnPropertyChanged("DisplayName");
        }

        bool IsNewVehicle
        {
            get { return !_carDealer.ContainsVehicle(_newCar); }
        }

        public string Error
        {
            get { return (_newCar as IDataErrorInfo).Error; }
        }

        public string this[string propertyName]
        {
            get
            {
                string error = (_newCar as IDataErrorInfo)[propertyName];

                // Dirty the commands registered with CommandManager,
                // such as our Save command, so that they are queried
                // to see if they can execute now.
                CommandManager.InvalidateRequerySuggested();

                return error;
            }
        }

    }
}