﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using Domain;

namespace CarDealer.ViewModels
{
    /// <summary>
    /// Author:Mathias
    /// </summary>
    public class AllPrivateCustomersViewModel : WorkspaceViewModel
    {
        private readonly Domain.CarDealer _carDealer;
        public Private SelectedCustomer { get; set; }
        RelayCommand _editContractsCommand;

        public ObservableCollection<CustomerPrivateViewModel> AllPrivateCustomers { get; private set; }

        public AllPrivateCustomersViewModel(Domain.CarDealer carDealer)
        {
            if (carDealer == null)
                throw new ArgumentNullException("carDealer");

            this._carDealer = carDealer;

            //_carDealer.CustomerAdded += this.onCustomerAdded;

            this.PopulateCustomers();

            base.DisplayName = "Show all P.C.";
        }

        void PopulateCustomers()
        {
            List<CustomerPrivateViewModel> all =
                (from cust in _carDealer.GetPrivateCustomers()
                 select new CustomerPrivateViewModel(cust, _carDealer)).ToList();

            //foreach (CustomerPrivateViewModel cvm in all)
            //    cvm.PropertyChanged += this.OnCustomerViewModelPropertyChanged;

            this.AllPrivateCustomers = new ObservableCollection<CustomerPrivateViewModel>(all);
            //this.AllPrivateCustomers.CollectionChanged += this.OnCollectionChanged;
        }


        public List<Private> Customers
        {
            get
            {
                return _carDealer.GetPrivateCustomers();
                
            }
            set{}
        }


        bool CanEditContracts
        {
            get { return SelectedCustomer != null; }
        }

        public ICommand EditContractsCommand
        {
            get
            {
                if (_editContractsCommand == null)
                {
                    _editContractsCommand = new RelayCommand(
                        param => this.EditLeasing(),
                        param => this.CanEditContracts
                        );
                }
                return _editContractsCommand;
            }
        }


        public void EditLeasing()
        {
            //Todo
            ShowContractsListViewModel vm = new ShowContractsListViewModel(SelectedCustomer, _carDealer);
            vm.Init();

            //base.OnPropertyChanged("DisplayName");
        }



        #region Event Handling Methods

        //void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        //{
        //    if (e.NewItems != null && e.NewItems.Count != 0)
        //        foreach (CustomerViewModel custVM in e.NewItems)
        //            custVM.PropertyChanged += this.OnCustomerViewModelPropertyChanged;

        //    if (e.OldItems != null && e.OldItems.Count != 0)
        //        foreach (CustomerViewModel custVM in e.OldItems)
        //            custVM.PropertyChanged -= this.OnCustomerViewModelPropertyChanged;
        //}

        //void OnCustomerViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        //{
        //    string IsSelected = "IsSelected";

        //}

        //void onCustomerAdded(object sender, CustomerAddedEventArgs e)
        //{
        //    var viewModel = new CustomerPrivateViewModel(e.NewCustomer, _carDealer);
        //    this.AllPrivateCustomers.Add(viewModel);
        //}

        #endregion // Event Handling Methods
    }
}