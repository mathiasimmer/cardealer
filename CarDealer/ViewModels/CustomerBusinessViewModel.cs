﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using Domain;

namespace CarDealer.ViewModels
{
    /// <summary>
    /// Author:Mathias
    /// </summary>
    public class CustomerBusinessViewModel : WorkspaceViewModel, IDataErrorInfo
    {
        private readonly Domain.CarDealer _carDealer;
        private readonly Business _newBusiness;
        RelayCommand _saveCommand;

        public CustomerBusinessViewModel(Business newBusiness, Domain.CarDealer carDealer)
        {

            if (newBusiness == null)
                throw new ArgumentNullException("newBusiness");

            if (carDealer == null)
                throw new ArgumentNullException("carDealer");

            _newBusiness = newBusiness;
            _carDealer = carDealer;

            base.DisplayName = Strings.CustomerBusinessViewModel_DisplayName;
        }

        public string CompanyName
        {
            get { return _newBusiness.CompanyName; }
            set
            {
                if (value == _newBusiness.CompanyName)
                    return;

                _newBusiness.CompanyName = value;

                base.OnPropertyChanged("CompanyName");
            }
        }

        public string ContactPerson
        {
            get { return _newBusiness.ContactPerson; }
            set
            {
                if (value == _newBusiness.ContactPerson)
                    return;

                _newBusiness.ContactPerson = value;

                base.OnPropertyChanged("ContactPerson");
            }
        }

        public string SeNo
        {
            get { return _newBusiness.SeNo; }
            set
            {
                if (value == _newBusiness.SeNo)
                    return;

                _newBusiness.SeNo = value;

                base.OnPropertyChanged("SeNo");
            }
        }

        public string Address
        {
            get { return _newBusiness.Address; }
            set
            {
                if (value == _newBusiness.Address)
                    return;

                _newBusiness.Address = value;

                base.OnPropertyChanged("Address");
            }
        }

        public int Phone
        {
            get { return _newBusiness.Phone; }
            set
            {
                if (value == _newBusiness.Phone)
                    return;
                _newBusiness.Phone = value;

                base.OnPropertyChanged("Phone");
            }
        }

        public int Fax
        {
            get { return _newBusiness.Fax; }
            set
            {
                if (value == _newBusiness.Fax)
                    return;
                _newBusiness.Fax = value;

                base.OnPropertyChanged("Fax");
            }
        }


        bool CanSave
        {
            get
            {
                return _newBusiness.IsValid;
            }
        }

        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        param => this.Save(),
                        param => this.CanSave
                        );
                }
                return _saveCommand;
            }
        }


        public void Save()
        {
            if (!_newBusiness.IsValid)
                throw new InvalidOperationException("CustomerBusinessViewModel: Cannot save - Customer invalid");

            if (this.IsNewCustomer)
                _carDealer.AddCustomer(_newBusiness);

            //base.OnPropertyChanged("DisplayName");
        }

        bool IsNewCustomer
        {
            get { return !_carDealer.ContainsCustomer(_newBusiness); }
        }

        public string Error
        {
            get { return (_newBusiness as IDataErrorInfo).Error; }
        }

        public string this[string propertyName]
        {
            get
            {
                string error = (_newBusiness as IDataErrorInfo)[propertyName];

                // Dirty the commands registered with CommandManager,
                // such as our Save command, so that they are queried
                // to see if they can execute now.
                CommandManager.InvalidateRequerySuggested();

                return error;
            }
        }
    }
}
