﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Data;
using CarDealer.ViewModels;
using CarDealer.Views;
using Domain;

namespace CarDealer.ViewModels
{
    /// <summary>
    /// Author:Mathias
    /// </summary>
    public class MainWindowViewModel : WorkspaceViewModel
    {
        #region Fields

        ReadOnlyCollection<CommandViewModel> _commands;

        ObservableCollection<WorkspaceViewModel> _workspaces;
        private readonly Domain.CarDealer _carDealer;

        #endregion // Fields

        #region Constructor

        public MainWindowViewModel(string customerDataFile, Domain.CarDealer carDealer)
        {
            base.DisplayName = Strings.MainWindowViewModel_DisplayName;

            _carDealer = carDealer;
        }

        #endregion // Constructor

        #region Commands

        /// <summary>
        /// Returns a read-only list of commands 
        /// that the UI can display and execute.
        /// </summary>
        public ReadOnlyCollection<CommandViewModel> Commands
        {
            get
            {
                if (_commands == null)
                {
                    List<CommandViewModel> cmds = this.CreateCommands();
                    _commands = new ReadOnlyCollection<CommandViewModel>(cmds);
                }
                return _commands;
            }
        }

        List<CommandViewModel> CreateCommands()
        {
            return new List<CommandViewModel>
            {
                
                new CommandViewModel(
                    Strings.MainWindowViewModel_Command_CreateNewPrivateCustomer,
                    new RelayCommand(param => this.CreateNewPrivateCustomer())),

                new CommandViewModel(
                     Strings.MainWindowViewModel_Command_CreateNewBusinessCustomer,
                     new RelayCommand(param=> this.CreateNewBusinessCustomer())),

                new CommandViewModel(
                    Strings.MainViewModel_Command_ViewAllPrivateCustomers,
                    new RelayCommand(param => this.ShowAllPrivateCustomers())),

                new CommandViewModel(
                    Strings.MainViewModel_Command_ViewAllBusinessCustomers,
                    new RelayCommand(param => this.ShowAllBusinessCustomers())),

                new CommandViewModel(
                    Strings.MainWindowViewModel_Command_CreateNewCar,
                    new RelayCommand(param => this.CreateNewCar())),

                new CommandViewModel(
                    Strings.MainWindowViewModel_Command_CreateNewTruck,
                    new RelayCommand(param => this.CreateNewTruck())),

                new CommandViewModel(
                    Strings.MainWindowViewModel_Command_ViewAllCars,
                    new RelayCommand(param => this.ShowAllVehiclesCar())),

                new CommandViewModel(
                    Strings.MainWindowViewModel_Command_ViewAllTruck,
                    new RelayCommand(param => this.ShowAllVehiclesTruck()))



            };
        }

        

        #endregion // Commands

        #region Workspaces

        /// <summary>
        /// Returns the collection of available workspaces to display.
        /// A 'workspace' is a ViewModel that can request to be closed.
        /// </summary>
        public ObservableCollection<WorkspaceViewModel> Workspaces
        {
            get
            {
                if (_workspaces == null)
                {
                    _workspaces = new ObservableCollection<WorkspaceViewModel>();
                    _workspaces.CollectionChanged += this.OnWorkspacesChanged;
                }
                return _workspaces;
            }
        }

        void OnWorkspacesChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.NewItems.Count != 0)
                foreach (WorkspaceViewModel workspace in e.NewItems)
                    workspace.RequestClose += this.OnWorkspaceRequestClose;

            if (e.OldItems != null && e.OldItems.Count != 0)
                foreach (WorkspaceViewModel workspace in e.OldItems)
                    workspace.RequestClose -= this.OnWorkspaceRequestClose;
        }

        void OnWorkspaceRequestClose(object sender, EventArgs e)
        {
            WorkspaceViewModel workspace = sender as WorkspaceViewModel;
            workspace.Dispose();
            this.Workspaces.Remove(workspace);
        }

        #endregion // Workspaces

        #region Private Helpers


        void CreateNewPrivateCustomer()
        {
            Private newPrivate = new Private();
            CustomerPrivateViewModel workspace = new CustomerPrivateViewModel(newPrivate, _carDealer);
            this.Workspaces.Add(workspace);
            this.SetActiveWorkspace(workspace);
        }

        void CreateNewBusinessCustomer()
        {
            Business newBusiness = new Business();
            CustomerBusinessViewModel workspace = new CustomerBusinessViewModel(newBusiness, _carDealer);
            this.Workspaces.Add(workspace);
            this.SetActiveWorkspace(workspace);
        }

        void ShowAllPrivateCustomers()
        {
            AllPrivateCustomersViewModel workspace =
                this.Workspaces.FirstOrDefault(vm => vm is AllPrivateCustomersViewModel) as AllPrivateCustomersViewModel;
            if (workspace == null)
            {
                workspace = new AllPrivateCustomersViewModel(_carDealer);
                this.Workspaces.Add(workspace);
            }
            this.SetActiveWorkspace(workspace);
        }


        void ShowAllBusinessCustomers()
        {
            AllBusinessCustomersViewModel workspace =
                this.Workspaces.FirstOrDefault(vm => vm is AllBusinessCustomersViewModel) as AllBusinessCustomersViewModel;
            if (workspace == null)
            {
                workspace = new AllBusinessCustomersViewModel(_carDealer);
                this.Workspaces.Add(workspace);
            }
            this.SetActiveWorkspace(workspace);
        }

        void ShowAllVehiclesCar()
        {
            AllVehiclesCarViewModel workspace =
                this.Workspaces.FirstOrDefault(vm => vm is AllVehiclesCarViewModel) as AllVehiclesCarViewModel;
            if (workspace == null)
            {
                workspace = new AllVehiclesCarViewModel(_carDealer);
                this.Workspaces.Add(workspace);
            }
            this.SetActiveWorkspace(workspace);

        }

        void ShowAllVehiclesTruck()
        {
            AllVehiclesTruckViewModel workspace =
                this.Workspaces.FirstOrDefault(vm => vm is AllVehiclesTruckViewModel) as AllVehiclesTruckViewModel;
            if (workspace == null)
            {
                workspace = new AllVehiclesTruckViewModel(_carDealer);
                this.Workspaces.Add(workspace);
            }
            this.SetActiveWorkspace(workspace);

        }


        void CreateNewCar()
        {
            Car newCar = new Car();
            VehicleCarViewModel workspace = new VehicleCarViewModel(newCar, _carDealer);
            this.Workspaces.Add(workspace);
            this.SetActiveWorkspace(workspace);
        }

        void CreateNewTruck()
        {
            Truck newTruck = new Truck();
            VehicleTruckViewModel workspace = new VehicleTruckViewModel(newTruck, _carDealer);
            this.Workspaces.Add(workspace);
            this.SetActiveWorkspace(workspace);
        }





        void SetActiveWorkspace(WorkspaceViewModel workspace)
        {
            Debug.Assert(this.Workspaces.Contains(workspace));

            ICollectionView collectionView = CollectionViewSource.GetDefaultView(this.Workspaces);
            if (collectionView != null)
                collectionView.MoveCurrentTo(workspace);
        }

        #endregion // Private Helpers  
    }
}