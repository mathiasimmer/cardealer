﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using CarDealer.Views;
using Domain;

namespace CarDealer.ViewModels
{
    /// <summary>
    /// Author:Mathias
    /// </summary>
    public class LeasingViewModel
    {
        private readonly Leasing _newLeasing;
        private readonly Domain.CarDealer _carDealer;
        private readonly Business _customer;

        public LeasingViewModel(Leasing newLeasing, Business customer, Domain.CarDealer carDealer)
        {
            this._newLeasing = newLeasing;
            this._carDealer = carDealer;
            this._customer = customer;
        }

        public void Init()
        {
            LeasingView view = new LeasingView();
            view.DataContext = this;
            view.Show();
        }

        public List<Truck> Vehicles
        {
            get { return _carDealer.GetTrucks(); }
        }

        public Truck SelectedVehicle { get; set; }

        public DateTime Date
        {
            get { return _newLeasing.Date; }
            set
            {
                if (value == _newLeasing.Date)
                    return;

                _newLeasing.Date = value;
                //base.OnPropertyChanged("Date");
            }
        }

        public int Rent
        {
            get { return _newLeasing.Rent; }
            set
            {
                if (value == _newLeasing.Rent)
                    return;

                _newLeasing.Rent = value;
                //base.OnPropertyChanged("Rent");
            }
        }

        public int Months
        {
            get { return _newLeasing.Months; }
            set
            {
                if (value == _newLeasing.Months)
                    return;
                _newLeasing.Months = value;
                //base.OnPropertyChanged("Months");
            }
        }



        bool CanSave
        {
            get
            {
                return SelectedVehicle != null;
            }
        }


        public RelayCommand _saveCommand { get; private set; }

        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        param => this.Save(),
                        param => this.CanSave
                        );
                }
                return _saveCommand;
            }
        }

        public void Save()
        {

            //if (!_newLeasing.IsValid)
            //    throw new InvalidOperationException("VehicleTruckViewModel: Cannot save - Truck invalid");
            _newLeasing.Vehicle = SelectedVehicle;
            _customer.AddLeasing(_newLeasing);

            //base.OnPropertyChanged("DisplayName");
        }
    }
}