﻿using System;
using System.Collections.Generic;
using Domain;

namespace CarDealer.ViewModels
{
    /// <summary>
    /// Author:Mathias
    /// </summary>
    public class AllVehiclesTruckViewModel : WorkspaceViewModel
    {
        private Domain.CarDealer _carDealer;

        public AllVehiclesTruckViewModel(Domain.CarDealer carDealer)
        {
            if (carDealer == null)
                throw new ArgumentNullException("carDealer");

            this._carDealer = carDealer;

            base.DisplayName = Strings.MainWindowViewModel_Command_ViewAllTruck;
        }

        public List<Truck> Vehicles
        {
            get { return _carDealer.GetTrucks(); }
        }
    }
}