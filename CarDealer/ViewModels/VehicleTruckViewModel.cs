﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using Domain;

namespace CarDealer.ViewModels
{
    /// <summary>
    /// Author:Mathias
    /// </summary>
    public class VehicleTruckViewModel : WorkspaceViewModel, IDataErrorInfo
    {
        private Truck _newTruck;
        private readonly Domain.CarDealer _carDealer;
        RelayCommand _saveCommand;

        public VehicleTruckViewModel(Truck newTruck, Domain.CarDealer carDealer)
        {
            _newTruck = newTruck;
            _carDealer = carDealer;

            base.DisplayName = Strings.MainWindowViewModel_Command_CreateNewTruck;
        }

        public string Model
        {
            get { return _newTruck.Model; }
            set
            {
                if (value == _newTruck.Model)
                    return;

                _newTruck.Model = value;

                base.OnPropertyChanged("Model");
            }
        }

        public int Price
        {
            get { return _newTruck.Price; }
            set
            {
                if (value == _newTruck.Price)
                    return;

                _newTruck.Price = value;

                base.OnPropertyChanged("Price");
            }
        }

        public string Color
        {
            get { return _newTruck.Color; }
            set
            {
                if (value == _newTruck.Color)
                    return;

                _newTruck.Color = value;

                base.OnPropertyChanged("Color");
            }
        }


        public string State
        {
            get { return _newTruck.State.ToString(); }
            set
            {
                if (value == _newTruck.State.ToString() || String.IsNullOrEmpty(value))
                    return;

                _newTruck.State = (VehicleState)Enum.Parse(typeof(VehicleState), value);

                base.OnPropertyChanged("State");

            }
        }

        public string[] StateOptions
        {
            get
            {

                var enumValues = Enum.GetValues(typeof(VehicleState));

                string[] values = enumValues.OfType<object>().Select(o => o.ToString()).ToArray();


                //string[] newValues = new string[values.Length + 1];
                //newValues[0] = Strings.Unspecified;
                //Array.Copy(values, 0, newValues, 1, values.Length);

                //return newValues;
                return values;
            }
        }


        bool CanSave
        {
            get
            {
                return !_newTruck.State.ToString().Equals("") && _newTruck.IsValid;
            }
        }

        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        param => this.Save(),
                        param => this.CanSave
                        );
                }
                return _saveCommand;
            }
        }


        public void Save()
        {
            if (!_newTruck.IsValid)
                throw new InvalidOperationException("VehicleTruckViewModel: Cannot save - Truck invalid");

            if (this.IsNewVehicle)
                _carDealer.AddVehicle(_newTruck);

            //base.OnPropertyChanged("DisplayName");
        }

        bool IsNewVehicle
        {
            get { return !_carDealer.ContainsVehicle(_newTruck); }
        }

        public string Error
        {
            get { return (_newTruck as IDataErrorInfo).Error; }
        }

        public string this[string propertyName]
        {
            get
            {
                string error = (_newTruck as IDataErrorInfo)[propertyName];

                // Dirty the commands registered with CommandManager,
                // such as our Save command, so that they are queried
                // to see if they can execute now.
                CommandManager.InvalidateRequerySuggested();

                return error;
            }
        }

    }
}