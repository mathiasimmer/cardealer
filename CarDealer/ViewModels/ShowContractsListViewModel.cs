﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using CarDealer.Views;
using Domain;

namespace CarDealer.ViewModels
{
    /// <summary>
    /// Author:Mathias
    /// </summary>
    public class ShowContractsListViewModel
    {
        private Private _customer;
        private Domain.CarDealer _carDealer;
        public Contract SelectedContract { get; set; }
        RelayCommand _addContractCommand;

        public ShowContractsListViewModel(Private customer, Domain.CarDealer carDealer)
        {
            this._customer = customer;
            this._carDealer = carDealer;
        }

        public void Init()
        {
            ShowContractsListView listView = new ShowContractsListView();
            listView.DataContext = this;
            listView.Show();
        }

        public List<Contract> Contracts
        {
            get { return _customer.Cars; }
        }

        bool CanAddContract
        {
            get { return true; }
        }

        public ICommand AddContractCommand
        {
            get
            {
                if (_addContractCommand == null)
                {
                    _addContractCommand = new RelayCommand(
                        param => this.AddContract(),
                        param => this.CanAddContract
                        );
                }
                return _addContractCommand;
            }
        }


        public void AddContract()
        {
            Contract newContract = new Contract();
            newContract.Date = DateTime.Now;
            ContractViewModel vm = new ContractViewModel(newContract, _customer, _carDealer);
            vm.Init();

        }


    }
}