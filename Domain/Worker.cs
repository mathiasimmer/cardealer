﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
/**
Simple class to demonstrate threading
*/
namespace Domain
{
    public class Worker
    {
        private CarDealer carDealer;
        private volatile bool running = true;

        public Worker(CarDealer carDealer)
        {
            this.carDealer = carDealer;
        }

        public void Monitor()
        {
            while (running)
            {
                Console.WriteLine("Number of cars in the system :" + carDealer.GetCars().Count);
                Console.WriteLine("Number of trucks in the system :" + carDealer.GetTrucks().Count);
                Console.WriteLine("Number of private customers in the system :" + carDealer.GetPrivateCustomers().Count);
                Console.WriteLine("Number of business customers in the system :" + carDealer.GetBusinessCustomers().Count);

                Thread.Sleep(10000);
            }
        }

        public void Stop()
        {
            running = false;
        }
    }
}
