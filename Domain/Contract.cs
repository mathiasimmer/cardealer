﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    /// <summary>
    /// Author:Nicholai
    /// </summary>
    [Serializable]
    public class Contract : IContract
    {
        public Contract()
        {
        }

        public Contract(IVehicle car, ICustomer customer)
        {
            Vehicle = car;
            Customer = customer;
            Date = DateTime.Now;

        }

        public ICustomer Customer
        {
            get;

            set;
        }

        public DateTime Date
        {
            get; set;
        }

        public IVehicle Vehicle
        {
            get;

            set;
        }
    }
}
