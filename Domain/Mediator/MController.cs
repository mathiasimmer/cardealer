﻿using System;
using System.Collections.Generic;

namespace Domain.Mediator
{
    /// <summary>
    /// Author:Nicholai
    /// </summary>
    class MController
    {
        List<string> Index = new List<string>();
        private Persistence.Persistence Persistence;

        public MController()
        {
            Persistence = new Persistence.Persistence();
        }

        public void SaveObject(Object o, string name)
        {
            Persistence.Save(o, name);
            if (Index == null || !Index.Contains(name))
            {
                Index.Add(name);
            }

            SaveEnd();
            
        }

        public void SaveEnd()
        {
            Persistence.Save(Index, "Index");
        }

        public void LoadData(out List<IVehicle> vehicles, out List<ICustomer> customers)
        {
            vehicles = new List<IVehicle>();
            customers = new List<ICustomer>();

            Index = (List<string>) Persistence.Load<Object>("Index");

            if(Index == null)
            {
                Index = new List<string>();
                return;
            }

            Object TempList;
            foreach (var i in Index)
            {
                TempList = Persistence.Load<Object>(i);

                if (TempList is List<IVehicle>)
                {
                    vehicles = (List<IVehicle>)TempList;
                }
                else if (TempList is List<ICustomer>)
                {
                    customers = (List<ICustomer>)TempList;
                }
            }
        }


    }
}
