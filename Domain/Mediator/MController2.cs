﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Persistent;

//Replacement class for MController - Using Entity Framework instead
namespace Domain.Mediator
{
    using System;
    using System.Collections.Generic;

    namespace Domain.Mediator
    {

        public class MController2
        {
            private DataAccess<Car> _carDbPersistence;
            private DataAccess<Truck> _truckDbPersistence;
            private DataAccess<Business> _businessDbPersistence;
            private DataAccess<Private> _privateDbPersistence;
            
            public MController2()
            {
                _carDbPersistence = new DataAccess<Car>();
                _truckDbPersistence = new DataAccess<Truck>();
                _businessDbPersistence = new DataAccess<Business>();
                _privateDbPersistence = new DataAccess<Private>();
            }


            public void LoadData(out List<IVehicle> vehicles, out List<ICustomer> customers)
            {
                customers = new List<ICustomer>();
                vehicles = new List<IVehicle>();

                //foreach (var car in _carDbPersistence.Collection.ToList())
                //{
                //    vehicles.Save(car);
                //}
                vehicles.AddRange(_carDbPersistence.List);
                vehicles.AddRange(_truckDbPersistence.List);
                customers.AddRange(_businessDbPersistence.List);
                customers.AddRange(_privateDbPersistence.List);

            }

            public void Save(ICustomer customer)
            {
                if (customer is Business)
                {
                    Business business = (Business) customer;
                    if (business.Id == 0)
                    {
                        _businessDbPersistence.Save(business);
                    }
                    else
                    {
                        _businessDbPersistence.Update(business);
                    }
                    
                    return;
                }

                if (customer is Private)
                {
                    Private _private = (Private)customer;
                    if (_private.Id == 0)
                    {
                        _privateDbPersistence.Save(_private);
                    }
                    else
                    {
                        _privateDbPersistence.Update(_private);
                    }
                    return;
                }

                throw new Exception("Unsupported object: " + customer);
            }

            public void Save(IVehicle vehicle)
            {
                if (vehicle is Car)
                {
                    Car car = (Car)vehicle;
                    if (car.Id == 0)
                    {
                        _carDbPersistence.Save(car);
                    }
                    else
                    {
                        _carDbPersistence.Update(car);
                    }
                    return;
                }

                if (vehicle is Truck)
                {
                    Truck truck = (Truck)vehicle;
                    if (truck.Id == 0)
                    {
                        _truckDbPersistence.Save(truck);
                    }
                    else
                    {
                        _truckDbPersistence.Update(truck);
                    }
                    return;
                }

                throw new Exception("Unsupported object: " + vehicle);
            }
        }
    }
}
