﻿
namespace Domain
{
    /// <summary>
    /// Author:Nicholai
    /// </summary>
    public interface ICustomer
    {

        int Id
        {
            get;
        }

        int Phone
        {
            get; set;
        }
        string Address
        {
            get; set;
        }

        void NewVehicleAddedEventHandler(object sender, IVehicle newvVehicle);
    }
}