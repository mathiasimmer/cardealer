﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Domain
{
    /// <summary>
    /// Author:Nicholai & Mathias
    /// </summary>
    [Serializable]
    public class Private : ICustomer, IDataErrorInfo
    {
        public List<Contract> Cars = new List<Contract>();

        public Private(int id)
        {
            Id = id;
        }

        public Private()
        {

        }

        public int Id
        {
            get;

            private set;
        }

        public string Address
        {
            get;

            set;
        }

        public void NewVehicleAddedEventHandler(object sender, IVehicle newvVehicle)
        {
            Console.WriteLine("Event caught: new vehicle added " + newvVehicle);
        }

        public int Phone
        {
            get;

            set;
        }

        public string Name
        {
            get;

            set;
        }

        public int Age
        {
            get;

            set;
        }

        public Gender Gender
        {
            get;

            set;
        }

        public void AddContract(Contract contract)
        {
            if(Cars == null)
            {
                Cars = new List<Contract>();
            }
            Cars.Add(contract);
        }

        public string this[string propertyName]
        {
            get { return this.GetValidationError(propertyName); }
        }

        string IDataErrorInfo.Error { get { return null; } }

        #region Validation

        /// <summary>
        /// Returns true if this object has no validation errors.
        /// </summary>
        public bool IsValid
        {
            get
            {
                //foreach (string property in ValidatedProperties)
                //    if (GetValidationError(property) != null)
                //        return false;

                //return true;

                //USING LINQ
                return ValidatedProperties.All(property => GetValidationError(property) == null);
            }
        }

        static readonly string[] ValidatedProperties =
        {
            "Name",
            "Address",
            "Phone",
            "Age"
        };

        string GetValidationError(string propertyName)
        {
            if (Array.IndexOf(ValidatedProperties, propertyName) < 0)
                return null;

            string error = null;

            switch (propertyName)
            {
                case "Name":
                    error = this.ValidateName();
                    break;

                case "Address":
                    error = this.ValidateAddress();
                    break;

                case "Phone":
                    error = this.ValidatePhone();
                    break;
                case "Age":
                    error = this.ValidateAge();
                    break;

                default:
                    Debug.Fail("Unexpected property being validated on Private: " + propertyName);
                    break;
            }

            return error;
        }

        string ValidateName()
        {
            if (IsStringMissing(this.Name))
            {
                return Strings.Customer_Error_MissingName;
            }
            return null;
        }

        string ValidateAddress()
        {
            if (IsStringMissing(this.Address))
            {
                return Strings.Customer_Error_MissingAddress;
            }

            if (!this.Address.Any(c => char.IsDigit(c)))
            {
                return Strings.Customer_Error_AddressInvalid;
            }

            return null;
        }

        string ValidatePhone()
        {
            if (this.Phone == 0)
            {
                return Strings.Customer_Error_MissingPhone;
            }
            return null;
        }

        string ValidateAge()
        {
            if (this.Age < 0 || this.Age > 100)
            {
                return Strings.Customer_Error_InvalidAge;
            }
            return null;
        }

        static bool IsStringMissing(string value)
        {
            return
                String.IsNullOrEmpty(value) ||
                value.Trim() == String.Empty;
        }

        #endregion // Validation

        public override string ToString()
        {
            return string.Format("Cars: {0}, Id: {1}, Phone: {2}, Address: {3}, Name: {4}, Age: {5}, Gender: {6}", Cars, Id, Phone, Address, Name, Age, Gender);
        }
    }
}
