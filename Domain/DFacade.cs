﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class DFacade
    {
        private static CarDealer Dealer = null;
        public static CarDealer CarDealerObject
        {
            get
            {
                if (Dealer == null)
                    Dealer = new CarDealer();
                   
                return Dealer;
            }
        }

        
    }
}
