﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Domain.Mediator.Domain.Mediator;

namespace Domain
{
    /// <summary>
    /// Author:Nicholai & Mathias
    /// </summary>
    public class CarDealer
    {
        private List<IVehicle> Vehicles;
        private List<ICustomer> Customers;


        public delegate void NewVehicleAdded(object sender, IVehicle newVehicle);
        public event NewVehicleAdded NewVehicleAddedEvent;

        MController2 MCon = new MController2();

        public CarDealer()
        {
            MCon.LoadData(out Vehicles, out Customers);
            foreach (var customer in Customers)
            {
                NewVehicleAddedEvent += customer.NewVehicleAddedEventHandler;
            }

            //Start Monitor thread
            Worker worker = new Worker(this);
            Thread workerThread = new Thread(worker.Monitor);
            workerThread.IsBackground = true;
            // Start the worker thread.
            workerThread.Start();

            try
            {
                var folderWatcher = new FolderWatcher(this);
            }
            catch (Exception e)
            {
                
            }

        }

        public void AddCustomer(ICustomer customer)
        {
            NewVehicleAddedEvent += customer.NewVehicleAddedEventHandler;
            Customers.Add(customer);
            //MCon.SaveObject(Customers, "Customers");
            MCon.Save(customer);
        }


        public ICustomer GetCustomer(int id)
        {
            //foreach (ICustomer c in Customers)
            //{
            //    if(id == c.Id)
            //    {
            //        return c;
            //    }
            //}
            //return null;

            //Using LINQ:
            return Customers.FirstOrDefault(c => id == c.Id);
        }

        public void AddVehicle(IVehicle vehicle)
        {
            Vehicles.Add(vehicle);
            //MCon.SaveObject(Vehicles, "Vehicles");
            MCon.Save(vehicle);

            NewVehicleAddedEvent?.Invoke(this, vehicle);
        }

        public void AddTVehicle(int id, string color, string model, int price, VehicleState state)
        {
            AddVehicle(new Truck(id, color, model, price, state));
        }

        public void AddCVehicle(int id, string color, string model, int price, VehicleState state)
        {
            AddVehicle(new Car(id, color, model, price, state));
        }

        public IVehicle GetVehicle(int id, Type clazz)
        {
            //foreach (IVehicle v in Vehicles && v.GetType() == clazz)
            //{
            //    if(id == v.Id)
            //    {
            //        return v;
            //    }
            //}
            //return null;

            //USING LINQ:
            return Vehicles.FirstOrDefault(v => id == v.Id && v.GetType() == clazz);
        }

        public bool ContainsCustomer(ICustomer newCustomer)
        {
            return Customers.Contains(newCustomer);
        }

        public List<Private> GetPrivateCustomers()
        {
            //List<Private> result = new List<Private>();

            //foreach (var cust in Customers)
            //{
            //    if (cust is Private)
            //    {
            //        result.Save((Private)cust);
            //    }
            //}

            //return result;

            //USING LINQ:
            return Customers.OfType<Private>().ToList();
        }

        public List<Business> GetBusinessCustomers()
        {
            //List<Business> result = new List<Business>();

            //foreach (var cust in Customers)
            //{
            //    if (cust is Business)
            //    {
            //        result.Save((Business)cust);
            //    }
            //}

            //return result;

            //USING LINQ:
            return Customers.OfType<Business>().ToList();
        }


        public List<Car> GetCars()
        {
            //List<Car> result = new List<Car>();

            //foreach (var vehicle in Vehicles)
            //{
            //    if (vehicle is Car)
            //    {
            //        result.Save((Car)vehicle);
            //    }
            //}

            //return result;


            //USING LINQ:
            return Vehicles.OfType<Car>().ToList();
        }

        public List<Truck> GetTrucks()
        {
            //List<Truck> result = new List<Truck>();

            //foreach (var vehicle in Vehicles)
            //{
            //    if (vehicle is Truck)
            //    {
            //        result.Save((Truck)vehicle);
            //    }
            //}

            //return result;

            //USING LINQ:
            return Vehicles.OfType<Truck>().ToList();
        }

        public void Save(IVehicle vehicle)
        {
            MCon.Save(vehicle);
        }

        public bool ContainsVehicle(IVehicle newCar)
        {
            return Vehicles.Contains(newCar);
        }
    
    }

    
}
