﻿
namespace Domain
{
    /// <summary>
    /// Author:Nicholai
    /// </summary>
    public interface IVehicle
    {

        int Id
        {
            get;
        }

        int Price
        {
            get; set;
        }

        string Color
        {
            get; set;
        }

        string Model
        {
            get;
        }

        VehicleState State
        {
            get; set;
        }

        
    }
}