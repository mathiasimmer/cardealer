﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    /// <summary>
    /// Author:Nicholai & Mathias
    /// </summary>
    [Serializable]
    public class Business : ICustomer, IDataErrorInfo
    {
        public List<Leasing> Trucks = new List<Leasing>();

        public Business(int id)
        {
            Id = id;
        }

        public Business()
        {

        }

        public int Id
        {
            get;

            private set;
        }

        public string Address
        {
            get;

            set;
        }

        public void NewVehicleAddedEventHandler(object sender, IVehicle newvVehicle)
        {
            Console.WriteLine("Event caught: new vehicle added " + newvVehicle);
        }

        public int Phone
        {
            get;

            set;
        }

        public string SeNo
        {
            get; set;
        }

        public int Fax
        {
            get;

            set;
        }

        public string ContactPerson
        {
            get;

            set;
        }

        public string CompanyName
        {
            get;

            set;
        }

        public void AddLeasing(Leasing leasing)
        {

            if(Trucks == null)
            {
                Trucks = new List<Leasing>();
            }
            Trucks.Add(leasing);
        }

        public string this[string propertyName]
        {
            get { return this.GetValidationError(propertyName); }
        }

        private string GetValidationError(string propertyName)
        {
            //TODO
            return null;
        }

        string IDataErrorInfo.Error { get { return null; } }

        //TODO
        public bool IsValid => true;


        public override string ToString()
        {
            return string.Format("Trucks: {0}, Id: {1}, Address: {2}, Phone: {3}, SeNo: {4}, Fax: {5}, CompanyName: {6}, ContactPerson: {7}", Trucks, Id, Address, Phone, SeNo, Fax, CompanyName, ContactPerson);
        }
    }
}
