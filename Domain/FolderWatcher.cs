﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Domain
{
    class FolderWatcher
    {
        private const String Importfolder = "ImportFolder";
        private FileSystemWatcher watcher;
        private CarDealer carDealer;

        public FolderWatcher(CarDealer carDealer)
        {
            this.carDealer = carDealer;
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + Importfolder))
            {
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + Importfolder);
            }

            //Initiate FileWatcher
            watcher = new FileSystemWatcher();
            //Set Folder Path
            watcher.Path = Importfolder;
            //Select only CSV files
            watcher.Filter = "*.csv";
            watcher.NotifyFilter = NotifyFilters.FileName;
            watcher.Created += watcher_FileCreated;
            watcher.EnableRaisingEvents = true;
        }

        void watcher_FileCreated(object sender, FileSystemEventArgs e)
        {

            Console.WriteLine("\t******A new import-file has been detected****");
            List<Truck> trucks = ParseFile(e.FullPath);

            Console.WriteLine(e.FullPath);

            Console.WriteLine("Imported the following Trucks:");

            foreach (var truck in trucks)
            {
                Console.WriteLine(truck);
                carDealer.AddVehicle(truck);
            }

            //Console.WriteLine("Deleting file...");
            //File.Delete(e.FullPath);
            //Console.WriteLine(e.FullPath + " has been successfully deleted!");
        }

        static List<Truck> ParseFile(String path)
        {
            String fileName = Path.GetFileNameWithoutExtension(path);
            Console.WriteLine(fileName);

            FileInfo fInfo = new FileInfo(@path);
            
            while (IsFileLocked(fInfo))
            {
                Thread.Sleep(500);
            }

            var reader = new StreamReader(@path);
            List<Truck> cars = new List<Truck>();
            int lineCount = 1;
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                try
                {
                    var values = line.Split(';');

                    int id = Convert.ToInt32(values[0]);
                    string color = values[1];
                    string model = values[2];
                    int price = Convert.ToInt32(values[3]);
                    VehicleState state = (VehicleState)Enum.Parse(typeof(VehicleState), values[4]);
                    cars.Add(new Truck(id, color, model, price, state));
                }
                catch (Exception e)
                {
                    Console.WriteLine(@"Could not parse line #" + lineCount);
                }

                lineCount++;
            }
            reader.Close();
            return cars;
        }

        static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;
            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }
    }
}
