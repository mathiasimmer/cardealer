﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    /// <summary>
    /// Author:Nicholai
    /// </summary>
    [Serializable]
    public enum VehicleState
    {
        Commissioned, Sold, Leased
    }
}
