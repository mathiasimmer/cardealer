﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    /// <summary>
    /// Author:Nicholai & Mathias
    /// </summary>
    [Serializable]
    public class Truck : IVehicle, IDataErrorInfo
    {

        public int Id { get; private set; }
        public string Color { get; set; }

        public string Model { get; set; }

        public int Price { get; set; }

        public VehicleState State { get; set; }

        public Truck(int id, string color, string model, int price, VehicleState state)
        {
            Id = id;
            Color = color;
            Model = model;
            Price = price;
            State = state;
        }

        public Truck()
        {

        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        public string this[string propertyName]
        {
            get { return this.GetValidationError(propertyName); }
        }

        private string GetValidationError(string propertyName)
        {
            //Todo
            return null;
        }

        //TODO
        string IDataErrorInfo.Error
        {
            get { return null; }
        }

        public bool IsValid
        {
            get { return true; }
        }

        public override string ToString()
        {
            return $"Id: {Id}, Color: {Color}, Model: {Model}, Price: {Price}, State: {State}";
        }
    }
}