﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    /// <summary>
    /// Author:Nicholai
    /// </summary>
    [Serializable]
    public class Leasing : IContract
    {

        public Leasing()
        {

        }

        public IVehicle Vehicle
        {
            get; set;
        }

        public ICustomer Customer
        {
            get;

            private set;
        }

        public DateTime Date
        {
            get; set;
        }

        public int Rent
        {
            get; set;
        }

        public int Months
        {
            get; set;
        }

        public Leasing(IVehicle vehicle, ICustomer customer, int rent, int months)
        {
            Customer = customer;
            Vehicle = vehicle;
            Rent = rent;
            Months = months;
            Date = DateTime.Now;
        }
    }
}
