﻿
using System;

namespace Domain
{
    /// <summary>
    /// Author:Nicholai
    /// </summary>
    public interface IContract
    {

        IVehicle Vehicle
        {
            get;
        }
        ICustomer Customer
        {
            get;
        }
        DateTime Date
        {
            get;
        }


    }
}
