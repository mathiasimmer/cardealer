﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistent
{
    public class DbPersistence<T> : DbContext where T : class
    {
        public DbPersistence() : base("name=CarDealer")
        {
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
            Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
        }
        public DbSet<T> Collection { get; set; }
    }
}
