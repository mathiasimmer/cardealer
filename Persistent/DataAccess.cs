﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistent
{
    public class DataAccess<T> where T : class
    {
        private DbPersistence<T> db;
        public DataAccess()
        {
            db = new DbPersistence<T>();
        }

        public List<T> List
        {
            get
            {
                System.Diagnostics.Debug.WriteLine("Getting list");
                return db.Collection.ToList();
            }
        }

        public void Save(T o)
        {
            System.Diagnostics.Debug.WriteLine("Saving: " + o);
            db.Collection.Add(o);
            db.SaveChanges();
        }

        public void Update(T o)
        {
            System.Diagnostics.Debug.WriteLine("Updating: " + o);
            db.Entry(o).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
