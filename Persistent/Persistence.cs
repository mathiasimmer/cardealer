﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace Persistence
{
    /// <summary>
    /// Author:Jeppe
    /// </summary>
    public class Persistence
    {
        public void Save(Object obj, String saveName)
        {
            Console.WriteLine("Saving " + saveName);
            saveName = saveName + ".bin";
            if (obj.GetType().IsSerializable)
            {
                Stream testFileStream = new FileStream(saveName, FileMode.Create);

                ////Fix for shit as bad xmlserializer implementation
                //List<Type> types = new List<Type>();
                //if(obj is IList && obj.GetType().IsGenericType)
                //{
                //    IList list = (IList)obj;
                //    foreach(Object lObj in list)
                //    {
                //        if (!types.Contains(lObj.GetType()))
                //        {
                //            types.Save(lObj.GetType());
                //        }
                //    }
                //}
                //XmlSerializer serializer = new XmlSerializer(obj.GetType(), types.ToArray());
                BinaryFormatter serializer = new BinaryFormatter();
                serializer.Serialize(testFileStream, obj);
                testFileStream.Close();
            }
        }

        public T Load<T>(string saveName)
        {
            Console.WriteLine("Loading " + saveName);
            saveName = saveName + ".bin";
            if (File.Exists(saveName))
            {
                Stream testFileStream = new FileStream(saveName, FileMode.Open);
                try
                {
                    //XmlSerializer deserializer = new XmlSerializer(typeof(T));
                    BinaryFormatter deserializer = new BinaryFormatter();
                    Object obj = deserializer.Deserialize(testFileStream);
                    if (obj is T)
                    {
                        T objT = (T)obj;
                        return objT;
                    }
                }
                catch (Exception e)
                {
                    testFileStream.Close();
                    throw;
                }
                finally
                {
                    testFileStream.Close();
                }
            }
            return default(T);
        }
    }
}
